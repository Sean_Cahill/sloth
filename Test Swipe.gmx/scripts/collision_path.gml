// collision_path(path, obj, prec, notme, stepsize)
//var path, obj, prec, notme, start, stepsize;
path  = argument0;
 xsp  = argument1;
ysp  = argument2;
obj   = argument3;
prec  = argument4;
notme = argument5;
stepsize = argument6;

start=0;
/*if (path_get_kind(path) == 0) {
  var num, i, collision;
  num = path_get_number(path);
  len = path_get_length(path);
  i = 0;
  repeat (num - 1 - start) {
    collision = collision_line(
      path_get_point_x(path, i),
      path_get_point_y(path, i),
      path_get_point_x(path, i+1),
      path_get_point_y(path, i+1),
      obj, prec, notme
    );
    if (collision) {
      global.pathpos = i/len;
      return collision;
    }
    i += 1;
  }
  return noone;
} else {*/
  var len, i, collision;
  len = path_get_length(path);
  i = 0;
  repeat ((len - start) / stepsize) {
    collision = collision_line(
      path_get_x(path, i / len),
      path_get_y(path, i / len),
      path_get_x(path, (i + stepsize) / len)+xsp,
      path_get_y(path, (i + stepsize) / len)+ysp,
      obj, prec, notme
    );
    if (collision) {
      global.pathpos = i/len;
      return collision;
    }
    i += stepsize;
  }
  return noone;
//}